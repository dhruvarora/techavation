from django.shortcuts import render
from django.views.generic import TemplateView
from oscar.apps.partner.models import StockRecord

class HomeView(TemplateView):

    template_name = "promotions/home.html"
    stocks = StockRecord.objects.all()[:10]

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {
        "stocks":self.stocks,
        })
