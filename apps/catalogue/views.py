from oscar.apps.catalogue import views as oscar_views
from oscar.apps.catalogue.models import Product, Category
from oscar.apps.partner.models import StockRecord
from django.shortcuts import get_object_or_404, render


class ProductCategoryView(oscar_views.ProductCategoryView):

    template_name = "catalogue/category.html"

    def get_category(self):
        if "pk" in self.kwargs:
            return get_object_or_404(Category, pk=self.kwargs['pk'])

    def get(self, request, category_slug, pk):
        products = Product.objects.filter(categories=self.get_category())
        p = []
        for each in products:
            srecord = StockRecord.objects.get(product=each)
            p.append(srecord)
        return render(request, self.template_name, {
        "category":self.get_category(),
        "products":products,
        "categories":Category.objects.all(),
        })


class ProductDetailView(oscar_views.ProductDetailView):

    def get(self, request, product_slug, pk):
        product = Product.objects.get(pk=pk)
        stocks = StockRecord.objects.all()[:10]

        return render(request, "catalogue/detail.html", {
        "stocks":stocks,
        "product":product,
        })
